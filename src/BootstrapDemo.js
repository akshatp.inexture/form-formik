import React from "react";
import { Alert } from "react-bootstrap";

const BootstrapDemo = () => {
  return (
    <>
      <div className="text-danger">BootstrapDemo</div>
      <Alert variant="danger">This is a alert—check it out!</Alert>
    </>
  );
};

export default BootstrapDemo;
