import "./App.css";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { Button } from "react-bootstrap";

function App() {
  const formvalidationschema = yup.object().shape({
    name: yup.string().required("name is required"),
    email: yup
      .string()
      .email("Please enter correct email")
      .required("Email is required"),
    gender: yup.string().required("Please select Gender"),
    password: yup
      .string()
      .required("Please Enter Password")
      .min(8, "Password is too short - should be 8 chars minimum."),
    phone: yup
      .number()
      .typeError("That doesn't look like a phone number")
      .positive("phone number can't start with a minus")
      .integer("phone number can't include a decimal point")
      .required("phone number is required"),
    date: yup.date().min(new Date("01-01-2019")).max(new Date()).required(),
    about: yup
      .string()
      .required("Address is required")
      .max(200, "Please Enter correct address"),
  });

  return (
    <div className="container">
      <h1> Form </h1>
      <Formik
        initialValues={{
          name: "",
          phone: "",
          password: "",
          gender: "",
          email: "",
          date: "",
          about: "",
        }}
        validationSchema={formvalidationschema}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        <Form>
          <div className="form-group">
            <label>Name:</label>
            <Field name="name" type="text" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="name" />
            </p>
          </div>
          <br /> <br />
          <div className="form-group">
            <label>Phone:</label>
            <Field name="phone" type="number" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="phone" />
            </p>
          </div>
          <br /> <br />
          <div className="form-group">
            <label>Password:</label>
            <Field name="password" type="password" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="password" />
            </p>
          </div>
          <br /> <br />
          <div className="form-group">
            <label>Email:</label>
            <Field name="email" type="text" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="email" />
            </p>
          </div>
          <br /> <br />
          <div>
            <label>Gender:</label>
            <br /> <br />
            <label>
              Male:
              <Field name="gender" value="male" type="radio" />
            </label>
            <label>
              Female:
              <Field name="gender" value="female" type="radio" />
            </label>
            <br />
            <p className="text-danger">
              <ErrorMessage name="gender" />
            </p>
          </div>
          <br /> <br />
          <div className="form-group">
            <label>Date:</label>
            <Field name="date" type="date" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="date" />
            </p>
          </div>
          <br /> <br />
          <div className="form-group">
            <label>About:</label>
            <Field name="about" as="textarea" className="form-control" />
            <p className="text-danger">
              <ErrorMessage name="about" />
            </p>
          </div>
          <br /> <br />
          <Button variant="outline-primary" type="submit">
            Submit
          </Button>
        </Form>
      </Formik>
    </div>
  );
}

export default App;
